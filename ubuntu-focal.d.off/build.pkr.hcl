build {
  sources = [ "hcloud.ubuntu-22_04-arm64",
              "hcloud.ubuntu-22_04-amd64" ]
  provisioner "shell" {
    inline = [
      "apt-get update"
    ]
    env = {
      BUILDER = "packer"
    }
  }
}

