source "hcloud" "ubuntu-22_04-amd64" {
  image         = "ubuntu-22.04"
  location      = "hel1"
  server_type   = "cx11"
  ssh_keys      = []
  user_data     = ""
  ssh_username  = "root"
  snapshot_name = "ubuntu-22_04-amd64"
  snapshot_labels = {
    base    = "ubuntu-22.04"
    version = "v1.0.0"
    name    = "ubuntu-22_04-amd64"
  }
}

source "hcloud" "ubuntu-22_04-arm64" {
  image         = "ubuntu-22.04"
  location      = "hel1"
  server_type   = "cax11"
  ssh_keys      = []
  user_data     = ""
  ssh_username  = "root"
  snapshot_name = "ubuntu-22_04-arm64"
  snapshot_labels = {
    base    = "ubuntu-focal"
    version = "v1.0.0"
    name    = "ubuntu-22_04-arm64"
  }
}
