packer {
  required_plugins {
    ansible = { source = "github.com/hashicorp/ansible", version = "~> 1.1" }
    hcloud  = { source = "github.com/hetznercloud/hcloud", version = "~> 1.2" }
  }
}
