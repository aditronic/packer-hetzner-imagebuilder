source "hcloud" "alma-9-amd64" {
  image         = "alma-9"
  location      = "hil"
  server_type   = "cpx11"
  ssh_keys      = []
  user_data     = ""
  ssh_username  = "root"
  snapshot_name = "alma-9-amd64"
  snapshot_labels = {
    base    = "alma-9"
    version = "v1.0.0"
    name    = "alma-9-amd64"
  }
}

source "hcloud" "alma-9-arm64" {
  image         = "alma-9"
  location      = "hel1"
  server_type   = "cax11"
  ssh_keys      = []
  user_data     = ""
  ssh_username  = "root"
  snapshot_name = "alma-9-arm64"
  snapshot_labels = {
    base    = "alma-9"
    version = "v1.0.0"
    name    = "alma-9-arm64"
  }
}
