build {
  sources = [
    # "hcloud.alma-9-arm64",
    "hcloud.alma-9-amd64"
  ]

  provisioner "shell" {
    inline = [
      "yum install -y epel-release",
      "yum install -y ansible",
      "yum update -y"
    ]
    env = {
      BUILDER = "packer"
    }
  }

  provisioner "ansible-local" {
    playbook_dir = "alma-9.d/ansible"
    playbook_file = "alma-9.d/playbook.yaml"
  }
}

